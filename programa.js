function casillas() {

    // crear 9 casillas
    let botones1;
    let botones2;
    let formulario;
    let i;
    let j;
    let salto1;
    let salto2;

    formulario = document.getElementById("tableroCal");

    for (j = 1; j < 3; j++) {
        botones1 = document.createElement("input");

        //Asignación de atributos
        botones1.setAttribute("type", "text");
        botones1.setAttribute("id", "boton" + j);
        botones1.setAttribute("class", "boton");
        formulario.appendChild(botones1);
        if (j % 2 == 0) {
            salto1 = document.createElement("br");
            formulario.appendChild(salto1);
        }
    }
    //creacion de botones de operaciones
    for (i = 1; i < 8; i++) {
        botones2 = document.createElement("input");

        //Asignación de atributos
        botones2.setAttribute("type", "button");
        botones2.setAttribute("id", "boton" + (i + 2));
        botones2.setAttribute("class", "boton2");
        formulario.appendChild(botones2);
        if (i % 3 == 0) {
            salto2 = document.createElement("br");
            formulario.appendChild(salto2);
        }
    }
    //asignacion de valores y el evento onclick
    document.getElementById("boton3").setAttribute("onclick", "calculador.sumar();");
    document.getElementById("boton4").setAttribute("onclick", "calculador.restar();");
    document.getElementById("boton5").setAttribute("onclick", "calculador.modular();");
    document.getElementById("boton6").setAttribute("onclick", "calculador.potencial();");
    document.getElementById("boton7").setAttribute("onclick", "calculador.logaritmox();");
    document.getElementById("boton8").setAttribute("onclick", "calculador.logaritmoy();");
    // asignacion de texto a los botones

    document.getElementById("boton3").value = "+";
    document.getElementById("boton4").value = "-";
    document.getElementById("boton5").value = "%";
    document.getElementById("boton6").value = "x^y";
    document.getElementById("boton7").value = "log x";
    document.getElementById("boton8").value = "log y";


}

//clase
class CalculadorAritmetico {
    numero1;
    numero2;
    //funcion para sumar
    sumar() {
            let suma;
            suma = parseInt(document.getElementById('boton1').value) + parseInt(document.getElementById('boton2').value);
            document.getElementById("boton9").value = suma;
            return suma;
        }
        //funcion para restar
    restar() {
        let resta;
        resta = parseInt(document.getElementById('boton1').value) - parseInt(document.getElementById('boton2').value);
        document.getElementById("boton9").value = resta;
        return resta;
    }

    // funcion modular
    modular() {
        let modulo;
        modulo = parseInt(document.getElementById('boton1').value) % parseInt(document.getElementById('boton2').value);
        document.getElementById("boton9").value = modulo;
        return modulo;
    }

    //funcion potencia
    potencial() {
        let potencia;
        potencia = Math.pow(parseInt(document.getElementById('boton1').value), parseInt(document.getElementById('boton2').value));
        document.getElementById("boton9").value = potencia;
        return potencia;
    }

    //funciones logaritmo
    logaritmox() {
        let log;
        log = Math.log10(parseInt(document.getElementById('boton1').value));
        document.getElementById("boton9").value = log;
        return log;
    }
    logaritmoy() {
        let logy;
        logy = Math.log10(parseInt(document.getElementById('boton2').value));
        document.getElementById("boton9").value = logy;
        return logy;
    }
}

//objeto

let calculador = new CalculadorAritmetico();